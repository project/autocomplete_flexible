<?php

declare(strict_types=1);

namespace Drupal\autocomplete_flexible\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a form element for an autocomplete flexible element.
 *
 * Specify either #default_value or #value but not both.
 *
 * Properties:
 * - #flexible_default_value: The initial value of the form element. JavaScript alter
 *   the value prior to submission.
 * - #flexible_options: Given possibility to override Javascript plugin options.
 * - #autocomplete_route_name: @see \Drupal\Core\Render\Element\FormElement
 * - #autocomplete_route_parameters: @see \Drupal\Core\Render\Element\FormElement
 *
 * @FormElement("autocomplete_flexible")
 */
class AutocompleteFlexible extends FormElement {

  /**
   * Value indicating a field accepts an unlimited number of values.
   */
  const CARDINALITY_UNLIMITED = -1;

  /**
   * The minimum number of characters to trigger autocomplete.
   */
  const MIN_LENGTH = 3;

  /**
   * The element classes.
   */
  const CLASS_HIDDEN = 'autocomplete-flexible-hidden';
  const CLASS_TEXTFIELD = 'autocomplete-flexible-textfield';
  const CLASS_WRAPPER = 'autocomplete-flexible-wrapper';
  const CLASS_SELECTED_LIST = 'autocomplete-flexible-selected-list';

  /**
   * The value separator.
   */
  const SEPARATOR = '|';

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $class = static::class;
    return [
      '#flexible_options' => [],
      '#process' => [
        [$class, 'processElement'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * Autocomplete Flexible element process callback.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   details element.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array &$element): array {
    if (!isset($element['#autocomplete_route_name'])) {
      throw new \LogicException('An autocomplete route name is needed.');
    }

    $defaultValues = $element['#flexible_default_value'] ?? [];
    foreach ($defaultValues as &$defaultValue) {
      if (!isset($defaultValue['value']) || !isset($defaultValue['label'])) {
        throw new \LogicException('The default values ​​must at least contain the "value" and "label" properties.');
      }
    }
    if (!is_array($defaultValues)) {
      $defaultValues = [$defaultValues => $defaultValues];
    }

    // Add label_display and label variables to template.
    static::buildElementLabel($element);

    static::buildElementAutocomplete($element);
    static::buildElementHidden($element, $defaultValues);

    $element['#after_build'][] = [static::class, 'afterBuild'];

    $options =
      ['separator' => static::SEPARATOR] +
      $element['#flexible_options'] +
      [
        'minLength' => static::MIN_LENGTH,
        'max' => static::CARDINALITY_UNLIMITED,
        'selectedListClass' => static::CLASS_SELECTED_LIST,
        'wrapperClass' => static::CLASS_WRAPPER,
      ];

    $element['#tree'] = TRUE;
    $element['#prefix'] = '<div class="' . $options['wrapperClass'] . '">';
    $element['#suffix'] = '</div>';

    $element['#attached']['library'][] = 'autocomplete_flexible/init';
    $element['#attached']['drupalSettings']['autocomplete_flexible'][$element['#name']] = [
      'values' => $defaultValues,
      'options' => $options,
    ];

    return $element;
  }

  /**
   * Build element "autocomplete".
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   details element.
   */
  protected static function buildElementAutocomplete(array &$element): void {
    $element['autocomplete'] = [
      '#type' => 'textfield',
      '#description' => $element['#description'] ?? '',
      '#attributes' => [
        'class' => [static::CLASS_TEXTFIELD],
      ],
      '#process' => [
        [static::class, 'processAutocomplete'],
      ],
    ];
    $element['autocomplete'] += array_intersect_key(
      $element,
      array_flip([
        '#id',
        '#autocomplete_route_name',
        '#autocomplete_route_parameters',
      ])
    );
  }

  /**
   * Build element "label".
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   details element.
   */
  protected static function buildElementLabel(array &$element): void {
    $element['label'] = ['#theme' => 'form_element_label'];
    $element['label'] += array_intersect_key(
      $element,
      array_flip([
        '#id',
        '#required',
        '#title',
        '#title_display',
      ])
    );
  }

  /**
   * Build element "hidden".
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   details element.
   * @param array $defaultValues
   *   The form element default values.
   */
  protected static function buildElementHidden(array &$element, array $defaultValues): void {
    $element['hidden'] = [
      '#type' => 'hidden',
      '#value' => implode(static::SEPARATOR, array_column($defaultValues, 'value')),
      '#attributes' => [
        'class' => [static::CLASS_HIDDEN],
      ],
    ];
    $element['hidden'] += array_intersect_key(
      $element,
      array_flip([
        '#id',
        '#required',
      ])
    );
  }

  /**
   * Form API after build callback for the duration parameter type form.
   *
   * Fixes up the form value by applying the multiplier.
   */
  public static function afterBuild(array $element, FormStateInterface $form_state): array {
    // By default Drupal sets the maxlength to 128 if the property isn't
    // specified, but since the limit isn't useful in some cases,
    // we unset the property.
    unset($element['autocomplete']['#maxlength']);

    $hiddenValue = trim($element['hidden']['#value']);
    $element['#value'] = [];

    if (!empty($hiddenValue)) {
      $element['#value'] = array_unique(
        explode(static::SEPARATOR, $hiddenValue)
      );
    }

    // Autocomplete flexible field must be converted from a two-element array
    // into a single value.
    $form_state->setValueForElement($element, $element['#value']);

    return $element;
  }

}
