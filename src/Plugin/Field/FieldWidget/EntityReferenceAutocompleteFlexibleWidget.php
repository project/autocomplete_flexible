<?php

declare(strict_types=1);

namespace Drupal\autocomplete_flexible\Plugin\Field\FieldWidget;

use Drupal\autocomplete_flexible\Element\AutocompleteFlexible;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Plugin implementation of the 'entity_reference_autocomplete_flexible' widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_autocomplete_flexible",
 *   label = @Translation("Autocomplete (Flexible style)"),
 *   description = @Translation("An autocomplete text field with flexible rendering support."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class EntityReferenceAutocompleteFlexibleWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
        'min_length' => AutocompleteFlexible::MIN_LENGTH,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = parent::settingsForm($form, $form_state);

    $element['min_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum number of characters'),
      '#default_value' => $this->getSetting('min_length'),
      '#min' => 0,
      '#description' => $this->t('The number of characters before triggering autocompletion.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $summary[] = $this->t(
      'Minimum number of characters: @min_length',
      ['@min_length' => $this->getSetting('min_length')]
    );
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(
    FieldItemListInterface $items,
    array &$form,
    FormStateInterface $form_state
  ): array {
    return $this->formFlexibleElement($items, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function formSingleElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ): array {
    return $this->formFlexibleElement($items, $form, $form_state);
  }

  /**
   * Build autocomplete flexible form element.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   An array of the field values. When creating a new entity this may be NULL
   *   or an empty array to use default values.
   * @param array $form
   *   An array representing the form that the editing element will be attached
   *   to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The autocomplete flexible form element.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function formFlexibleElement(
    FieldItemListInterface $items,
    array &$form,
    FormStateInterface $form_state
  ): array {
    $field_name = $this->fieldDefinition->getName();
    $selection_settings = $this->getFieldSetting('handler_settings') + [
        'match_operator' => $this->getSetting('match_operator'),
        'match_limit' => $this->getSetting('match_limit'),
      ];
    $target_type = $this->getFieldSetting('target_type');

    $data = serialize($selection_settings) .
      $target_type .
      $this->getFieldSetting('handler');
    $selection_settings_key = Crypt::hmacBase64($data, Settings::getHashSalt());

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name, 'hidden']);
    $key_exists = NULL;
    $value = NestedArray::getValue($form_state->getUserInput(), $path, $key_exists);

    if ($key_exists && !empty($value)) {
      $entity_ids = [];

      $values = explode(AutocompleteFlexible::SEPARATOR, $value);
      foreach ($values as $input) {
        $match = EntityAutocomplete::extractEntityIdFromAutocompleteInput($input);

        if ($match !== NULL) {
          $entity_ids[] = (int)$match;
        }
      }

      $entities = \Drupal::entityTypeManager()->getStorage($target_type)
        ->loadMultiple($entity_ids);
    }
    else {
      $entities = $items->referencedEntities();
    }

    $default_values = [];
    foreach ($entities as $entity) {
      $label = $entity->label();

      // Allow modules to alter the field widget form element.
      $context = [
        'entity' => $entity,
        'field_name' => $field_name,
      ];
      \Drupal::moduleHandler()->alter(
        'hook_autocomplete_flexible_widget_label',
        $label,
        $form_state,
        $context
      );

      $value = $entity->label() . ' ('. $entity->id() .')';
      $default_values[$value] = ['value' => $value, 'label' => $label];
    }

    return [
      '#type' => 'autocomplete_flexible',
      '#title' => $this->fieldDefinition->getLabel(),
      '#title_display' => 'before',
      '#description' => $this->getFilteredDescription(),
      '#size' => $this->getSetting('size'),
      '#required' => $this->fieldDefinition->isRequired(),
      '#placeholder' => $this->getSetting('placeholder'),
      '#autocomplete_route_name' => 'system.entity_autocomplete',
      '#autocomplete_route_parameters' => [
        'target_type' => $target_type,
        'selection_handler' => $this->getFieldSetting('handler'),
        'selection_settings_key' => $selection_settings_key,
      ],
      '#flexible_default_value' => $default_values,
      '#flexible_options' => [
        'minLength' => $this->getSetting('min_length'),
        'max' => $this->fieldDefinition->getFieldStorageDefinition()->getCardinality(),
      ],
      '#selection_settings' => $selection_settings,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $trusted_values = [];

    foreach ($values as $input) {
      $match = EntityAutocomplete::extractEntityIdFromAutocompleteInput($input);

      if ($match !== NULL) {
        $trusted_values[] = [
          'target_id' => (int) $match,
        ];
      }
    }

    return $trusted_values;
  }

  /**
   * Form element validation handler for entity_autocomplete elements.
   */
  public static function validateEntityAutocomplete(
    array &$element,
    FormStateInterface $form_state,
    array &$complete_form
  ): void {
    $values = [];

    $form_state->setValueForElement($element, $values);
  }

}
