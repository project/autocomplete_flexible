/**
 * @file
 * Autocomplete flexible plugin.
 */

/**
 * Cardinality maximum (no limit !).
 *
 * @type {Number}
 */
const AF_CARDINALITY_MAX = -1;

/**
 * Default settings:
 *   - autocompleteDisabledValue: Define a value when autocomplete input is disabled.
 *   - max: The max values of the form element.
 *   - minLength: The minimum of characters to trigger autocomplete.
 *   - selectedListClass: The selected list wrapper class.
 *   - wrapperClass: The form element wrapper class.
 *
 * @type {Object}
 */
const AF_DEFAULTS = {
  max: AF_CARDINALITY_MAX,
  minLength: 3,
  separator: "|",
  autocompleteDisabledValue: "",
  duplicationErrorMessage: Drupal.t("This item has already been added."),
  buttonRemoveClass: "autocomplete-flexible-button-delete",
  selectedListClass: "autocomplete-flexible-selected-list",
  wrapperClass: "autocomplete-flexible-wrapper",
};

/**
 * The plugin initialization class.
 * Added to wrapper on plugin initialization.
 *
 * @type {String}
 */
const AF_PLUGIN_INIT_CLASS = "autocomplete-flexible-init";

/**
 * Local object for method references and define script meta-data.
 *
 * @return {Object}
 *   The autocomplete flexible plugin.
 */
// eslint-disable-next-line no-unused-vars
let AutocompleteFlexible = class {

  #autocomplete = {};
  #message = false;
  #settings = {};
  #values = {};

  $inputAutocomplete;
  $inputHidden;
  $wrapper;
  $selectedValues = false;

  /**
   * Plugin constructor.
   *
   * @param {jQuery|HTMLElement} $inputAutocomplete
   *  The input autocomplete element.
   * @param {jQuery|HTMLElement} $inputHidden
   *  The input hidden element.
   * @param {Object} options
   *   Custom settings (override AF_DEFAULTS).
   * @param {Object[]} values
   *   The available values to append to the bottom of field (each value must to contains 2 properties: value, label).
   */
  constructor($inputAutocomplete, $inputHidden, options, values) {
    Object.assign(this.#settings, AF_DEFAULTS, options);
    Object.assign(this.#values, values);

    this.$inputAutocomplete = $inputAutocomplete;
    this.$inputHidden = $inputHidden;
    this.$wrapper = $inputHidden.parents("." + this.settings.wrapperClass);

    // Clone Drupal autocomplete.
    Object.assign(this.#autocomplete, Drupal.autocomplete);
    this.#autocomplete.options.minLength = this.settings.minLength;
    this.#autocomplete.options.renderItem = this.renderItem;
    this.#autocomplete.options.select = this.selectHandler;
  }

  /**
   * Append an item.
   *
   * @param {Object} item
   *   The selected item.
   *
   * @private
   */
  #appendItem(item) {
    const that = this;

    const $item = jQuery("<li>" +
      Drupal.theme("autocompleteFlexibleSelectedItem", item) +
      Drupal.theme("autocompleteFlexibleRemoveButton", {
        "class": this.settings.buttonRemoveClass,
        "value": item.value
      }) +
      "</li>");

    this.$selectedValues.append($item);

    // Add value in input hidden field.
    this.#values[item.value] = item;
    this.$inputHidden.val(Object.keys(this.#values).join(this.settings.separator));

    // Disable input autocomplete.
    this.#disableInputAutocomplete();

    // Manage item removing.
    jQuery("." + this.settings.buttonRemoveClass, $item).on("click", event => {
      event.preventDefault();

      const value = jQuery(event.target).attr("data-autocomplete-flexible-value");

      that.#removeItem(that.#values[value]);
    });
  }

  /**
   * Disable input autocomplete.
   *
   * @private
   */
  #disableInputAutocomplete() {
    if (
      this.settings.max !== AF_CARDINALITY_MAX &&
      Object.keys(this.values).length >= this.settings.max
    ) {
      this.$inputAutocomplete.prop("disabled", true);

      // Add a description text.
      this.$inputAutocomplete.val(this.settings.autocompleteDisabledValue);
    }
  }

  /**
   * Display default values.
   *
   * @private
   */
  #displayDefaultValues() {
    const that = this;

    // Init selected list.
    if (!this.$selectedValues) {
      this.$selectedValues = jQuery("<ul class=\"" + this.settings.selectedListClass + "\"></ul>");
      this.$wrapper.append(this.$selectedValues);
    }

    Object.entries(this.values).forEach(([value, item]) => {
      // Prevent "value" entry doesn't exist.
      item.value = value;
      that.#values[value].value = value;

      that.#appendItem(item);
    });
  }

  /**
   * Enable input autocomplete.
   *
   * @private
   */
  #enableInputAutocomplete() {
    if (
      (
        this.settings.max === AF_CARDINALITY_MAX ||
        Object.keys(this.values).length < this.settings.max
      ) &&
      this.$inputAutocomplete.prop("disabled")
    ) {
      this.$inputAutocomplete.prop("disabled", false);

      // Remove description text.
      this.$inputAutocomplete.val("");
    }
  }

  /**
   * Initialize plugin.
   */
  init() {
    // Make sure to initialize the plugin only once for this element.
    if (
      this.$wrapper.length === 0 ||
      this.$wrapper.hasClass(AF_PLUGIN_INIT_CLASS)
    ) {
      return;
    }

    this.$wrapper.addClass(AF_PLUGIN_INIT_CLASS);

    // Initialize autocomplete management.
    this.#initAutocomplete();

    this.#initMessage();

    // Display default values.
    this.#displayDefaultValues();

    // Make sure autocomplete input is empty.
    this.$inputAutocomplete.val("");

    // Disable input autocomplete if max values is reached.
    this.#disableInputAutocomplete();
  }

  /**
   * Initialize autocomplete management.
   *
   * @private
   */
  #initAutocomplete() {
    const that = this;

    // Destroy Drupal Core autocomplete on this field.
    this.$inputAutocomplete.autocomplete("destroy");

    // Manage autocomplete flexible engine.
    this.$inputAutocomplete.autocomplete(this.#autocomplete.options).each(function () {
      jQuery(this).data("ui-autocomplete")._renderItem = that.#autocomplete.options.renderItem;
    });
    this.$inputAutocomplete.on("compositionstart.autocomplete", () => {
      that.#autocomplete.options.isComposing = true;
    });
    this.$inputAutocomplete.on("compositionend.autocomplete", () => {
      that.#autocomplete.options.isComposing = false;
    });

    // Manage autocomplete selection.
    this.$inputAutocomplete.on("autocompleteselect", (event, ui) => {
      // Clear old message.
      that.#message.clear();

      // Check value is already added.
      if (
        that.settings.duplicationErrorMessage &&
        that.values[ui.item.value] !== undefined
      ) {
        that.#message.add(that.settings.duplicationErrorMessage, {
          type: "error"
        });
        return;
      }

      // Append value in display list.
      that.#appendItem(ui.item);
    });
  }

  /**
   * Initialize message management.
   *
   * @private
   */
  #initMessage() {
    const messageWrapper = document.createElement("div");
    this.$wrapper.prepend(messageWrapper);

    this.#message = new Drupal.Message(messageWrapper);
  }

  /**
   * Remove an item.
   *
   * @param {Object} item
   *   The item to remove.
   *
   * @private
   */
  #removeItem(item) {
    // Remove item in input hidden value.
    delete this.#values[item.value];
    this.$inputHidden.val(Object.keys(this.values).join(this.settings.separator));

    // Remove item in selected list.
    jQuery("." + this.settings.buttonRemoveClass + "[data-autocomplete-flexible-value=\"" + item.value + "\"]")
      .parent("li")
      .remove();

    // Enable input autocomplete.
    this.#enableInputAutocomplete();
  }

  /**
   * Override jQuery UI _renderItem function to output HTML by default.
   *
   * @param {jQuery} ul
   *   jQuery collection of the ul element.
   * @param {Object} item
   *   The list item to append.
   *
   * @return {jQuery}
   *   jQuery collection of the ul element.
   */
  renderItem(ul, item) {
    // Give possibility to customize item result.
    return jQuery("<li>").append(jQuery("<a>").html(
      Drupal.theme("autocompleteFlexibleItemResult", item)
    )).appendTo(ul);
  }

  /**
   * Handles an autocompleteselect event.
   *
   * @param {jQuery.Event} event
   *   The event triggered.
   * @param {Object} ui
   *   The jQuery UI settings object.
   *
   * @return {Boolean}
   *   Returns false to indicate the event status.
   *
   * @see https://api.jqueryui.com/autocomplete/#event-select
   */
  // eslint-disable-next-line no-unused-vars
  selectHandler(event, ui) {
    // Return false to tell jQuery UI that we've filled in the value already.
    return false;
  }

  /**
   * Get current settings (readonly).
   *
   * @return {Object}
   *   Current settings.
   */
  get settings() {
    return this.#settings;
  }

  /**
   * Get current values (readonly).
   *
   * @return {Object[]}
   *   Current values.
   */
  get values() {
    return this.#values;
  }

}

/**
 * Render autocomplete flexible item result.
 * It's possible to return any HTML rendering.
 *
 * @param {Object} item
 *   An item result.
 *
 * @return {String|HTMLElement}
 *   The item result rendering.
 */
Drupal.theme.autocompleteFlexibleItemResult = (item) => {
  // By default, return the value itself.
  return item.label;
}


/**
 * Render autocomplete flexible item remove button.
 * It's possible to return any HTML rendering.
 *
 * @param {Object} settings
 *   The remove button settings.
 *
 * @return {String|HTMLElement}
 *   The remove button rendering.
 */
Drupal.theme.autocompleteFlexibleRemoveButton = (settings) => {
  return "<button class=\"" + settings.class + "\" data-autocomplete-flexible-value=\"" + settings.value + "\">" +
    Drupal.t("Remove") +
    "</button>";
}

/**
 * Render autocomplete flexible selected item.
 * It's possible to return any HTML rendering.
 *
 * @param {Object} item
 *   The selected item.
 *
 * @return {String|HTMLElement}
 *   The selected value rendering.
 */
Drupal.theme.autocompleteFlexibleSelectedItem = (item) => {
  // By default, return the value itself.
  return item.label;
}
