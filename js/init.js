/**
 * @file
 * Flexible autocomplete initialize behavior.
 */

((AutocompleteFlexible, Drupal, $) => {

  Drupal.behaviors.autocompleteFlexibleInitialize = {
    attach(context, settings) {
      const autocompleteFlexibleInstances = settings.autocomplete_flexible || {};

      Object.entries(autocompleteFlexibleInstances).forEach(([name, instance]) => {
        // Need to use jQuery selector for jQuery UI Autocomplete.
        const $inputHidden = $("input[name=\"" + name + "[hidden]\"]", context);
        const $inputAutocomplete = $("input[name=\"" + name + "[autocomplete]\"]", context);

        if (
          $inputHidden.length === 0 ||
          $inputAutocomplete.length === 0 ||
          typeof instance.instance !== "undefined"
        ) {
          return false;
        }

        instance.instance = new AutocompleteFlexible(
          $inputAutocomplete,
          $inputHidden,
          instance.options,
          instance.values
        );
        instance.instance.init();
      });
    }
  };
})(AutocompleteFlexible, Drupal, jQuery);
