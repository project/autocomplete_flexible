/**
 * @file
 * Theming autocomplete flexible widget.
 */

/**
 * Render item into results.
 *
 * @param {Element} item
 *   The result item to render.
 *
 *
 * @return {string}
 *   The result item rendered.
 */
Drupal.theme.autocompleteFlexibleItemResult = (item) => {
  // Override default rendering in your theme.
  // It's not possible to override default rendering into a module.
  // Default rendering is defining in widget.
  // @see: autocomplete_flexible/js/plugin/autocomplete-flexible.js
  let html = "<p><strong>" + item.label + "</strong></p>";

  if (typeof item.published !== "undefined") {
    html += "<p>" +
      (
        item.published ?
          "✔️" + (item.type === "user" ? Drupal.t("Active") : Drupal.t("Published"))
          :
          "❌️" + (item.type === "user" ? Drupal.t("Blocked") : Drupal.t("Not published"))
      ) +
      "</p>";
  }

  return html;
};

/**
 * Render selected item.
 *
 * @param {Element} item
 *   The selected item element.
 *
 * @return {string}
 *   The selected item rendered.
 */
Drupal.theme.autocompleteFlexibleSelectedItem = (item) => {
  // Override default rendering in your theme.
  // It's not possible to override default rendering into a module.
  // Default rendering is defining in widget.
  // @see: autocomplete_flexible/js/plugin/autocomplete-flexible.js
  let html = "";

  if (item.created && item.changed) {
    html += "<p>🕐&nbsp;" +
      Drupal.t("@label1 - @label2", {
        "@label1": Drupal.t("Created: @date", {"@date": new Date(item.created).toLocaleString()}),
        "@label2": Drupal.t("Changed: @date", {"@date": new Date(item.changed).toLocaleString()}),
      }) +
      "</p>";
  }
  else if (item.created) {
    html += "<p>🕐️" +
      Drupal.t("Created: @date", {"@date": new Date(item.created).toLocaleString()}) +
      "</p>";
  }
  else if (item.changed) {
    html += "<p>🕐️" +
      Drupal.t("Changed: @date", {"@date": new Date(item.changed).toLocaleString()}) +
      "</p>";
  }

  if (item.type !== item.bundle) {
    html += "<p>ℹ️" +
      Drupal.t("@label1 - @label2", {
        "@label1": item.typeLabel,
        "@label2": item.bundleLabel,
      }) +
      "</p>";
  }
  else {
    html += "<p>ℹ️" + item.typeLabel + "</p>";
  }

  html += "<p><strong>" + item.label + "</strong></p>";

  if (typeof item.published !== "undefined") {
    html += "<p>" +
      (
        item.published ?
          "✔️" + Drupal.t("Published")
          :
          "❌️" + Drupal.t("Not published")
      ) +
      "</p>";
  }

  return html;
};

/**
 * Render remove button.
 *
 * @param {object} settings
 *   The button settings.
 *
 * @return {string}
 *   The button rendered.
 */
Drupal.theme.autocompleteFlexibleRemoveButton = (settings) => {
  // Override default rendering in your theme.
  // It's not possible to override default rendering into a module.
  // Default rendering is defining in widget.
  // @see: autocomplete_flexible/js/plugin/autocomplete-flexible.js
  return "<button class=\"" + settings.class + " button button--secondary\" data-autocomplete-flexible-value=\"" + settings.value + "\">" +
    Drupal.t("Remove") +
    "</button>";
};
