<?php

declare(strict_types=1);

namespace Drupal\autocomplete_flexible_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

final class ExampleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'autocomplete_flexible_examples_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $this->buildElement($form, 'node');
    $this->buildElement($form, 'user');

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    // Attach widget theming into form.
    // In a theme context, prefer to extend "autocomplete_flexible/plugin" library.
    $form['#attached']['library'][] = 'autocomplete_flexible_examples/theme';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $nids = $form_state->getValue('node');
    $uids = $form_state->getValue('user');
    dump($nids, $uids);

    \Drupal::messenger()->addStatus(
      $this->t(
        'The selected nodes are: :nids.',
        [':nids' => implode(', ', $nids)],
      )
    );
    \Drupal::messenger()->addStatus(
      $this->t(
        'The selected users are: :uids.',
        [':uids' => implode(', ', $uids)],
      )
    );
  }

  /**
   * @param array $form
   *
   * @param string $entity_type
   *   An entity type.
   */
  private function buildElement(array &$form, string $entity_type): void {
    $form[$entity_type] = [
      '#type' => 'autocomplete_flexible',
      '#title' => $this->t('Autocomplete flexible (@entity_type)', ['@entity_type' => $entity_type]),
      // Define your custom autocomplete route to add several entity fields.
      '#autocomplete_route_name' => 'autocomplete_flexible_examples.autocomplete',
      // Add parameters to your custom route if it needs them.
      '#autocomplete_route_parameters' => ['target_type' => $entity_type],
      '#required' => TRUE,
      '#flexible_options' => [
        // Start autocomplete from 2 characters.
        'minLength' => 2,
        // Limit field values to 3 items.
        'max' => 3,
        // Define your custom remove button class.
        'buttonRemoveClass' => 'button-custom-remove-class',
        // Define your custom selected list class.
        'selectedListClass' => 'list-custom-select-class',
        // Define your custom wrapper class (or override it into theme).
        'wrapperClass' => 'wrapper-custom-class',
      ],
    ];
  }

}
