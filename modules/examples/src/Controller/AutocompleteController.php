<?php

declare(strict_types=1);

namespace Drupal\autocomplete_flexible_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class AutocompleteController extends ControllerBase {

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The entity bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Constructs an AutocompleteController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database Service Object.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The bundle information.
   */
  public function __construct(
    Connection $database,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
  ) {
    $this->database = $database;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * Get custom autocomplete response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object that contains the typed tags.
   *
   * @param string $target_type
   *   The entity target type.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The autocomplete response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function autocomplete(Request $request, string $target_type): JsonResponse {
    $matches = [];

    // Get the typed string from the URL, if it exists.
    $input = $request->query->get('q');

    // Check this string for emptiness, but allow any non-empty string.
    if (is_string($input) && mb_strlen($input)) {
      $storage = $this->entityTypeManager()->getStorage($target_type);

      $fieldLabel = $this->entityTypeManager()->getDefinition($target_type)
        ->getKey('label');

      $entityIds = $storage->getQuery()
        ->condition(
          $fieldLabel,
          '%' . $this->database->escapeLike($input) . '%',
          'LIKE'
        )
        ->accessCheck(TRUE)
        ->execute();

      $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($target_type);
      foreach($storage->loadMultiple($entityIds) as $entity) {
        // Add all the fields you need to prepare for rendering.
        $match = [
          'id' => $entity->id(),
          'bundle' => $entity->bundle(),
          'bundleLabel' => $bundleInfo[$entity->bundle()]['label'],
          'label' => $entity->label(),
          'type' => $entity->getEntityTypeId(),
          'typeLabel' => $entity->getEntityType()->getLabel(),
        ];

        if ($entity instanceof EntityChangedInterface) {
          $match['changed'] = date(
            'Y-m-d\TH:i:s',
            (int) $entity->getChangedTime()
          );
        }
        if ($entity instanceof EntityPublishedInterface) {
          $match['published'] = $entity->isPublished();
        }
        if (method_exists($entity, 'getCreatedTime')) {
          $match['created'] = date(
            'Y-m-d\TH:i:s',
            (int) $entity->getCreatedTime()
          );
        }

        $matches[] = $match;
      }
    }

    return new JsonResponse($matches);
  }

}
