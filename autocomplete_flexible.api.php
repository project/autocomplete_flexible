<?php

/**
 * @file
 * Autocomplete Flexible API documentation.
 */

declare(strict_types=1);

use Drupal\Core\Form\FormStateInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter autocomplete flexible widget item label provided by other modules.
 *
 * @param string $label
 *   The selected item label.
 * @param $form_state
 *   The current state of the form.
 * @param $context
 *   An associative array containing the following key-value pairs:
 *   - entity: The selected entity object.
 *     \Drupal\Core\Entity\EntityInterface object.
 *   - field_name: The name of the field being displayed.
 */
function hook_autocomplete_flexible_widget_label(
  string &$label,
  FormStateInterface $form_state,
  array $context
): void {
  $label = $context['entity']->label();
}

/**
 * @} End of "addtogroup hooks".
 */
